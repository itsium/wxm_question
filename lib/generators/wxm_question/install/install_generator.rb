module WxmQuestion
  module Generators
    class InstallGenerator < Rails::Generators::Base

      class_option :auto_run_migrations, :type => :boolean, :default => false

      source_root File.expand_path("../../../../../", __FILE__)

      def copy_ui
        directory 'public/ui', 'public/question'
      end

      def add_javascripts
        append_file 'app/assets/javascripts/application.js', "//= require wxm_question\n\n"
      end

      def add_stylesheets
        inject_into_file 'app/assets/stylesheets/application.css', " *= require wxm_question\n", :before => /\*\//, :verbose => true
      end

      def add_migrations
        run 'bundle exec rake railties:install:migrations'
      end

      def run_migrations
        run_migrations = options[:auto_run_migrations] || ['', 'y', 'Y'].include?(ask 'Would you like to run the migrations now? [Y/n]')
        if run_migrations
          run 'bundle exec rake db:migrate'
        else
          puts 'Skipping rake db:migrate, don\'t forget to run it!'
        end
      end
    end
  end
end
