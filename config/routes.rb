Rails.application.routes.draw do
    resources :questions do
        get 'vote1'
        get 'vote2'
    end
    get 'qapp/:appuid', to: 'questions#mobileshow'
end
