class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :title
      t.string :text1
      t.string :text2
      t.string :count1
      t.string :count2
      t.boolean :show_stat_before, default: false
      t.references :weiapp, index: true

      t.timestamps
    end
  end
end
