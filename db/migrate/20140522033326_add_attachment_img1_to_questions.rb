class AddAttachmentImg1ToQuestions < ActiveRecord::Migration
  def self.up
    change_table :questions do |t|
      t.attachment :img1
      t.attachment :img2
    end
  end

  def self.down
    drop_attached_file :questions, :img1
    drop_attached_file :questions, :img2
  end
end
