class ChangeCountType < ActiveRecord::Migration
  def up
    change_column :questions, :count1, :integer, :default => 0
    change_column :questions, :count2, :integer, :default => 0
  end

  def down
    change_column :questions, :count1, :string
    change_column :questions, :count2, :string
  end
end
