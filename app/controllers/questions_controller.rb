class QuestionsController < InheritedResources::Base
    before_filter :wx_auth, except: [:wxlogin, :mobileshow]
    before_filter :check_user, only: [ :create, :update, :edit ]
    layout "mobile", only: [:mobileshow]

    def mobileshow
        @question = Weiapp.where(appuid: params[:appuid]).first().app
    end


    def show
        @question = Question.find(params[:id])
    end

    def create
        @question = Question.new(permitted_params[:question])
        @question.user = current_user
        create!
    end

    def update
        @question = Question.find(params[:id])
        @question.user = current_user
        update!
    end

    def destroy
        destroy! { a_url }
    end

    def edit
        @question = Question.find(params[:id])
        @qr_link = "#{root_url}#{@question.weiapp.appuid}"
        @qr_for_link = gen_qrcode(@qr_link)
    end

    def vote1
        @question = Question.find(params[:question_id])
        if @question.count1
            @question.count1 += 1
        else
            @question.count1 = 1
        end
        @question.save
    end

    def vote2
        @question = Question.find(params[:question_id])
        if @question.count2
            @question.count2 += 1
        else
            @question.count2 = 1
        end
        @question.save
    end

protected
    def permitted_params
        # TODO SET CORRECT FILTER
        params.permit!
    end
end
