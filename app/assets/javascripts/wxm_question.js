$( document ).ready(function(){
      var easyPieChartDefaults = {
      animate: 2000,
      scaleColor: false,
      lineWidth: 6,
      lineCap: 'square',
      size: 90,
      trackColor: '#e5e5e5'
    }
    $('#stat-btn').click(function(){
      $('#easy-pie-chart-1').easyPieChart($.extend({}, easyPieChartDefaults, {
        barColor: "#77b7c5"
      }));
      $('#easy-pie-chart-2').easyPieChart($.extend({}, easyPieChartDefaults, {
        barColor: "#77b7c5"
      }));
    });

});
