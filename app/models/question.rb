class Question < ActiveRecord::Base
    has_one :weiapp, as: :app, dependent: :destroy
    has_one :user, :through => :weiapp

    has_attached_file :img1, :styles => { :original => "600x600>", :thumb => "130x130>" }, :default_url => "/images/:style/question_missing.png"
    validates_attachment_content_type :img1, :content_type => /\Aimage\/.*\Z/
    has_attached_file :img2, :styles => { :original => "600x600>", :thumb => "130x130>" }, :default_url => "/images/:style/question_missing.png"
    validates_attachment_content_type :img2, :content_type => /\Aimage\/.*\Z/

  def display_name
    self.title
  end

  def display_description
    display_name
  end

  def identifier
    :question
  end

  def purcent1
    if (self.count1 != 0 or self.count2 != 0)
      p1 = self.count1 * 100 / (self.count1 + self.count2)
    else
      p1 = 0
    end
  end

  def purcent2
    if (self.count1 != 0 or self.count2 != 0)
      p2 = self.count2 * 100 / (self.count1 + self.count2)
    else
      p2 = 0
    end
  end

  def as_json(options=nil)
    item = super({ only: [
        :id, :title, :show_stat_before
    ] }.merge(options || {}))

    count1 = self.count1 || 0
    count2 = self.count2 || 0
    total = count1 + count2

    answers = [{
      text: self.text1,
      count: count1,
      image: self.img1.url(:original),
      thumb: self.img1.url(:thumb)
    }, {
      text: self.text2,
      count: count2,
      image: self.img2.url(:original),
      thumb: self.img2.url(:thumb)
    }]

    item.merge({
      total: total,
      answers: answers
    })
  end

  def to_builder
    Jbuilder.new do |question|
      question.(self, :id, :title, :show_stat_before)

      count1 = self.count1 || 0
      count2 = self.count2 || 0
      total = count1 + count2

      answers = [{
        text: self.text1,
        count: count1,
        image: self.img1.url(:original),
        thumb: self.img1.url(:thumb)
      }, {
        text: self.text2,
        count: count2,
        image: self.img2.url(:original),
        thumb: self.img2.url(:thumb)
      }]

      question.total total
      question.answers answers
    end
  end
end
